This script creates a csv-file with all waypoints from a userprofile on hikr.org,
including the name of the waypoint, coordinates, height and type of the point.

To make sure the script is working, better take the username from the url since
when a user changed his name, it might differ from the one in the url.

The script is installs all packages needed automatically, but therefor pip package installer
musst be installed.
