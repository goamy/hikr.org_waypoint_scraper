#!/usr/bin/env python3

"""
This script creates a csv_file with all waypoints from a userprofile on hikr.org, including the
name of the waypoint, coordinates, height and type of the point.

Author: Daniel Hadwiger
Copyright: Daniel Hadwiger, 2020
License: GNU GPLv3
Version: 1.0
Email: daniel.hadwiger@gmx.de
"""


import subprocess
import sys
import csv
import re
import os


def install(package):
    subprocess.check_call([sys.executable, "-m", "pip", "install", package])


try:
    from bs4 import *
except ImportError:
    print("Installing BeautifulSoup")
    install("bs4")
finally:
    from bs4 import *

try:
    import requests
except ImportError:
    print("Installing requests")
    install("requests")
finally:
    import requests


def parser(url):
    """
    parses html-code
    """
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    return soup


def get_waypointdata_from_tour(html):
    """
    creates a list of lists with name, latitude, longitude, height and type of every waypoint in a post
    """
    result = []
    tmp_name = re.findall("(?<=piz_name:\").*(?=\")", html.text)
    tmp_lat = re.findall("(?<=piz_lat:)\d*.\d*", html.text)
    tmp_lon = re.findall("(?<=piz_lon:)\d*.\d*", html.text)
    tmp_height = re.findall("(?<=piz_height:)\d*", html.text)
    tmp_type = re.findall("(?<=piz_type:\").*(?=\")", html.text)
    for x in range(len(tmp_name)):
        result.append([tmp_name[x], float(tmp_lat[x]), float(tmp_lon[x]), int(tmp_height[x]), tmp_type[x]])
    return result


def extract_links_from_journalpage(url_journal_page):
    """
    creates a list with all posts on a page of the journal
    """
    soup = parser(url_journal_page)
    tmp = []
    list_tours = []
    for element in soup.find_all(href=re.compile("www.hikr.org/tour/post")):
        tmp.append(element)
    for element in tmp:
        is_a_tour = re.findall("https\:\/\/www\.hikr\.org\/tour\/post\d*\.html", str(element))
        if is_a_tour[0] not in list_tours:
            list_tours.append(is_a_tour[0])
    return list_tours


def get_journalpages(url_journal):
    """
    creates a list with all journalpages of a user
    """
    page_list = [url_journal]
    soup = parser(url_journal)
    for page in page_list:
        for element in soup.find_all(id="NextLink"):
            page_list.append(element["href"])
            url2 = page_list[-1]
            soup = parser(url2)
    return page_list


def get_all_tours(url_journal):
    """
    creates a list with all posts in a journal
    """
    journal_list = []
    tmp = []
    for page in get_journalpages(url_journal):
        tmp.append(extract_links_from_journalpage(page))
    for x in tmp:
        for xx in x:
            journal_list.append(xx)
    return journal_list


status = 0
while status == 0:
    username = input("\nEnter the USERNAME (https://www.hikr.org/user/USERNAME/) here: ")
    if requests.get("https://www.hikr.org/user/%s/" % username).status_code < 400:
        status = 1
    else:
        print("This username doesn’t exist. Make sure to take the username from the url")

print("\ngetting waypoints...\n")

tmp = []
for element in get_all_tours("https://www.hikr.org/user/%s/tour/" % username):
    tmp.append(get_waypointdata_from_tour(parser(element)))

waypoint_list = []

for x in tmp:
    for xx in x:
        if xx not in waypoint_list:
            waypoint_list.append(xx)

try:
    os.mkdir("waypoints")
except FileExistsError:
    pass

with open(os.path.join(os.getcwd(), "waypoints", "hikr_waypoints_%s.csv" % username), 'w', newline='') as csvfile:
    result = csv.writer(csvfile, delimiter=",")
    result.writerow(["waypoint", "latitude", "longitude", "height", "type_of_waypoint"])
    for waypoint in waypoint_list:
        result.writerow(waypoint)

print(f"Finished scraping. Wrote file \'hikr_waypoints_{username}.csv\'")
